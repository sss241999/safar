package com.example.metro;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.metro.DataWareHouse.Registeration;
import com.example.metro.DataWareHouse.RegisteratoinLab;
import com.google.android.gms.tasks.OnSuccessListener;

public class RegisterPage extends AppCompatActivity {
    public static final String userDetails = "com.example.metro.userDetails";
    private EditText RuserIdFld,
            RnameFld, RpasswordFld,
            RageFld, RadrssFld,
            RmobileNoFld, RcardNoFld;

    private Button btnCapture;
    private ImageView imgCapture;
    private static final int Image_Capture_Code = 1;
    private Bitmap img = null;

    /*Button btn_capture;
    ImageView iv_image;
    int code = 1;*/

    private Button RsubmitBtn;

    public boolean isValid(String s) {
        return (!s.trim().isEmpty());
    }

    public boolean userIdIsValid(String s) {
        return (s.length() >= 5 && isValid(s));
    }

    public boolean passIsValid(String s) {
        return (s.length() >= 5 && isValid(s));
    }


    public int C2I(String st) {
        return Integer.parseInt(st);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        RuserIdFld = (EditText) findViewById(R.id.RuserIdFld);
        RnameFld = (EditText) findViewById(R.id.RnameFld);
        RpasswordFld = (EditText) findViewById(R.id.RpasswordFld);
        RadrssFld = (EditText) findViewById(R.id.RaddressFld);
        RageFld = (EditText) findViewById(R.id.RageFld);
        RmobileNoFld = (EditText) findViewById(R.id.RmobileNoFld);
        RcardNoFld = (EditText) findViewById(R.id.RcardNoFld);
        RsubmitBtn = (Button) findViewById(R.id.RsubmitBtn);
        btnCapture =(Button)findViewById(R.id.btnTakePicture);
        imgCapture = (ImageView) findViewById(R.id.capturedImage);

        RsubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userIdIsValid(RuserIdFld.getText().toString())
                        && isValid(RnameFld.getText().toString())
                        && passIsValid(RpasswordFld.getText().toString())
                        && isValid(RadrssFld.getText().toString())
                        && isValid(RageFld.getText().toString())
                        && isValid(RmobileNoFld.getText().toString())
                        && isValid(RcardNoFld.getText().toString())) {

                    //getting te date and put it into model
                    Registeration register = new Registeration();
                    register.setUuid(C2I(RuserIdFld.getText().toString()));
                    register.setUserName(RnameFld.getText().toString());
                    register.setPassword(RpasswordFld.getText().toString());
                    register.setAddress(RadrssFld.getText().toString());
                    register.setAge(C2I(RageFld.getText().toString()));
                    register.setMobileNo(C2I(RmobileNoFld.getText().toString()));
                    register.setCardNo(C2I(RcardNoFld.getText().toString()));

                    Intent intent = new Intent(v.getContext(), SelectPage.class);
                    intent.putExtra(userDetails, register);
                    Toast.makeText(RegisterPage.this, register.getUserName(), Toast.LENGTH_SHORT).show();
                    boolean b = RegisteratoinLab.get(RegisterPage.this).insert(register);
                    if (b == true) {
                        Toast.makeText(RegisterPage.this, "Registered Successfully", Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(v.getContext(), "Data Already Registered \n try again!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(v.getContext(), "please Fill Fields Right", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getApplicationContext().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(cInt,Image_Capture_Code);
                    } else {
                        requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Image_Capture_Code) {
            if (resultCode == RESULT_OK) {
                img = (Bitmap) data.getExtras().get("data");
                Toast.makeText(this, "Image Stored", Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
        }
    }
}

